import NavBar from "./Components/NavBar";
import ReactLearning from "./Components/ReactLearning";
import AddUser from "./Components/AddUser";
import AllUser from "./Components/AllUser";
import NonFound from "./Components/NonFound";
import {Route, Routes} from "react-router-dom";
import EditUser from "./Components/EditUser";

function App() {
  return (
    <div className="App">
        <NavBar />
        <Routes>
            <Route path="/" element={<ReactLearning />} />
            <Route path="/all" element={<AllUser />} />
            <Route path="/add" element={<AddUser />} />
            <Route path="/edit/:id" element={<EditUser />} />
            <Route path="/*" element={<NonFound />} />

        </Routes>
    </div>
  );
}

export default App;
