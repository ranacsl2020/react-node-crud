import {deleteUser, getUsers,} from "../Service/api";
import {useEffect, useState} from "react";
import {Button, makeStyles, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import { Link} from "react-router-dom";

const useStyle = makeStyles({
    table:{
        width: '90%',
        margin: '50px 0 0 50px'
    },
    thead:{
        '& > *':{
            background: '#000000',
            color: '#ffffff',
            fontSize:'16px'
        }
    }
})
const AllUser = () => {
    const classes =useStyle();
    const [users, setUsers] = useState([]);
    useEffect(() =>{
        getAllusers();
    },[])

    const getAllusers =  async () => {
      const response =  await getUsers();
      console.log(response)
        setUsers(response.data)
    }
    const deleteUserData = async (id) =>{
      await deleteUser(id);
      getAllusers();
    }
    return(
        <table className={classes.table}>
            <TableHead >
                <TableRow className={classes.thead}>
                    <TableCell>id</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>UserName</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Phone</TableCell>
                    <TableCell>Action</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {
                    users.map(user =>(
                        <TableRow>
                            <TableCell>{user.id}</TableCell>
                            <TableCell>{user.name}</TableCell>
                            <TableCell>{user.username}</TableCell>
                            <TableCell>{user.email}</TableCell>
                            <TableCell>{user.phone}</TableCell>
                            <TableCell>
                                <Button variant='contained' color='primary' style={{marginRight :10}} component={Link} to={`/edit/${user.id}`}>Edit</Button>
                                <Button variant='contained' color='secondary' style={{marginRight :10}} onClick={() => deleteUserData(user.id)}>Delete</Button>
                            </TableCell>
                        </TableRow>
                    ))
                }
            </TableBody>
        </table>
    )
}
export default AllUser;