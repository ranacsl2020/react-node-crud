import {AppBar, Toolbar, Typography, makeStyles} from "@material-ui/core";
import {NavLink} from "react-router-dom";

const useStyle = makeStyles({
    header:{
        background: "skyblue"
    },
    tabs:{
        textDecoration: 'none',
        marginLeft: 16,
    },
    active:{
        color: '#f1f1f1',
        textDecoration: 'none',
        marginLeft: 16,
    }
})
const NavBar = ()=> {
    const classes = useStyle();
    return (
        <AppBar className={classes.header} position="static">
            <Toolbar>
                <NavLink className={(navData) => navData.isActive ? classes.active :classes.tabs} to="./"> React Learning</NavLink>
                <NavLink className={(navData) => navData.isActive ? classes.active :classes.tabs} to="all"> All Users</NavLink>
                <NavLink className={(navData) => navData.isActive ? classes.active :classes.tabs} to="add"> Add User</NavLink>

            </Toolbar>
        </AppBar>
    )
}
export default NavBar;