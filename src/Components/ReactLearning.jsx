
import dashboard from '../Assets/Images/dashboard.jpg'
import dashboardRight from '../Assets/Images/dashboard1.jpg'
import {makeStyles} from "@material-ui/core";
const dashboardStyle = makeStyles({
    image: {
        width:'50%',
        height:'50%',
            }
})
const ReactLearning = ()=>{
    const classes = dashboardStyle();
    return(
        <div>
            <h4>React Learning</h4>
            <div style={{disable:'flex'}}>
                <img src={dashboard} className={classes.image}/>
                <img src={dashboardRight} className={classes.image}/>

            </div>
        </div>
    )
}
export default ReactLearning;