import notFund  from '../Assets/Images/NotFund.jpg';
const NonFound = () =>{
    return(
        <div>
            <img src={notFund} style={{width: '30%', margin: '80px 0 0 35%' }}/>
        </div>
    )
}
export default NonFound;