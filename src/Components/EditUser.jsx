import {Button, FormControl, FormGroup, Input, InputLabel, makeStyles, Typography} from "@material-ui/core";
import {useEffect, useState} from "react";
import {editUser, getUsers} from "../Service/api";
import { useNavigate, useParams } from "react-router-dom";
const useStyle = makeStyles({
    container : {
        width : '50%',
        margin: '5% 0 0 25%',
        '& > *' : {
            marginTop : 20
        }
    }
})
const initialValues = {
    name : '',
    username: '',
    email:'',
    phone:''
}

const EditUser = ()=> {

    const classes = useStyle();
    const [user,setUser] = useState(initialValues);
    const {name, username, email, phone} = user;
    const {id} = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        loadUserData()
    },[])
    const loadUserData = async () => {
       const response = await getUsers(id);
       setUser(response.data)
    }
    const onValueChange = (e) =>{
        setUser({...user,[e.target.name] : e.target.value})
    }
    const editUserDetails = async () => {
        await editUser(id, user)
        navigate("/all");
    }
    return(
        <FormGroup className={classes.container}>
            <Typography variant='h4'> Edit User</Typography>
            <FormControl>
                <InputLabel>Name</InputLabel>
                <Input onChange={(e) =>onValueChange(e)} name={"name"} value={name}/>
            </FormControl>
            <FormControl>
                <InputLabel>User Name</InputLabel>
                <Input onChange={(e) =>onValueChange(e)} name={"username"} value={username}/>
            </FormControl>
            <FormControl>
                <InputLabel>Email</InputLabel>
                <Input onChange={(e) =>onValueChange(e)} name={"email"} value={email}/>
            </FormControl>
            <FormControl>
                <InputLabel>Phone No</InputLabel>
                <Input onChange={(e) =>onValueChange(e)} name={"phone"} value={phone}/>
            </FormControl>
            <Button onClick={() => editUserDetails()} variant={"contained"} color='primary'>Edit User</Button>
        </FormGroup>
    )
}
export default EditUser;